import './styles.css';
import React, { useState } from 'react';

import EventDashboard from '../../features/events/eventDashboard/EventDashboard';
import NavBar from '../../features/nav/NavBar.jsx';
import { Container } from 'semantic-ui-react';
import { Route } from "react-router-dom";
import HomePage from "../../features/home/HomePage";
import EventDetailedPage from '../../features/events/eventDetailed/EventDetailedPage';
import EventForm from '../../features/events/eventForm/EventForm';

const App = () => {

  return (
    <>
      <Route exact path="/" component={HomePage} />
      <Route path={'/(.+)'} render={() => (
        <>
          <NavBar />
          <Container className="main">
            <Route exact path="/events" component={EventDashboard} />
            <Route exact path="/events/:id" component={EventDetailedPage} />
            <Route exact path={["/createEvent", "/manage/:id"]} component={EventForm} />
          </Container>
        </>
      )} />
    </>
  );
}

export default App;
